package TestCSVFileReadLocally.TestCSVFileReadLocally.resources;

import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class FileController {
    @GetMapping("/read-csv-file")
    public List<List<String>> readCsvFile() throws IOException {
        FileSystemResource imgFile = new FileSystemResource("C:\\AAAAAAAAAA\\12.csv");

        InputStream inputStream = imgFile.getInputStream();

        List<List<String>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");

                records.add(Arrays.asList(values));
            }
        }

        return records;
    }

    @GetMapping("/remove-csv-file")
    public void removeCsvFile() throws IOException {
        Files.delete(Paths.get("C:\\AAAAAAAAAA\\12.csv"));
    }
}