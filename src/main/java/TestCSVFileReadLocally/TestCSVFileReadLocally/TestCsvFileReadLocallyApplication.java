package TestCSVFileReadLocally.TestCSVFileReadLocally;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCsvFileReadLocallyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCsvFileReadLocallyApplication.class, args);
	}

}
